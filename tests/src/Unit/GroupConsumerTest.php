<?php

declare(strict_types=1);

namespace Drupal\Tests\authorization_group\Unit;

use Drupal\authorization\AuthorizationProfileInterface;
use Drupal\authorization_group\Plugin\authorization\Consumer\GroupConsumer;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Storage\GroupRoleStorageInterface;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the GroupConsumer class.
 *
 * @group authorization_group
 */
class GroupConsumerTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The group membership loader.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $membershipLoader;

  /**
   * The group storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $groupStorage;

  /**
   * The group role storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $groupRoleStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The profile.
   *
   * @var \Drupal\authorization\AuthorizationProfileInterface
   */
  protected $profile;

  /**
   * The group consumer.
   *
   * @var \Drupal\authorization_group\Plugin\authorization\Consumer\GroupConsumer
   */
  protected $consumer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();
    $string_translation = $this->getStringTranslationStub();
    $container->set('string_translation', $string_translation);

    $this->membershipLoader = $this->prophesize('\Drupal\group\GroupMembershipLoaderInterface');
    $container->set('group.membership_loader', $this->membershipLoader->reveal());

    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $container->set('entity_type.manager', $this->entityTypeManager->reveal());

    $this->groupStorage = $this->prophesize('\Drupal\Core\Entity\Sql\SqlEntityStorageInterface');
    $this->entityTypeManager->getStorage('group')
      ->willReturn($this->groupStorage->reveal());

    $this->groupRoleStorage = $this->prophesize(GroupRoleStorageInterface::class);
    $this->entityTypeManager->getStorage('group_role')
      ->willReturn($this->groupRoleStorage->reveal());

    $this->messenger = $this->prophesize('Drupal\Core\Messenger\MessengerInterface');
    $container->set('messenger', $this->messenger->reveal());

    $this->profile = $this->prophesize(AuthorizationProfileInterface::class);
    \Drupal::setContainer($container);

    $configuration = [
      'profile' => $this->profile->reveal(),
      'delete_membership' => FALSE,
    ];

    $this->consumer = GroupConsumer::create(
      $container,
      $configuration,
      'authorization_group',
      [
        'label' => t('Groups'),
        'id' => 'group',
      ]);
  }

  /**
   * Tests the buildConfigurationForm method.
   */
  public function testBuildConfigurationForm() {

    $form_state = $this->prophesize(FormStateInterface::class);
    $form = $this->consumer->buildConfigurationForm([], $form_state->reveal());

    $this->assertIsArray($form);
    $this->assertCount(1, $form);
    $this->assertArrayHasKey('delete_membership', $form);
  }

  /**
   * Tests the buildRowForm method.
   */
  public function testBuildRowForm() {
    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $group->label()
      ->willReturn('Group 1')
      ->shouldBeCalled($this->once());
    $group_role_1 = $this->prophesize('\Drupal\group\Entity\GroupRole');
    $group_role_1->isInternal()
      ->willReturn(FALSE)
      ->shouldBeCalled($this->once());

    $group_role_2 = $this->prophesize('\Drupal\group\Entity\GroupRole');
    $group_role_2->isInternal()
      ->willReturn(TRUE)
      ->shouldBeCalled($this->once());
    $group_role_2->isAnonymous()
      ->willReturn(FALSE)
      ->shouldBeCalled($this->once());
    $group_role_2->id()
      ->willReturn('role2')
      ->shouldBeCalled($this->once());
    $group_role_2->label()
      ->willReturn('Role 2')
      ->shouldBeCalled($this->once());
    $group_type = $this->prophesize('\Drupal\group\Entity\GroupType');
    $group_type->getRoles()
      ->willReturn([$group_role_1->reveal(), $group_role_2->reveal()])
      ->shouldBeCalled($this->once());
    $group->getGroupType()
      ->willReturn($group_type->reveal())
      ->shouldBeCalled($this->once());
    $this->groupStorage->loadMultiple()
      ->willReturn([1 => $group->reveal()])
      ->shouldBeCalled($this->once());
    $this->profile->getConsumerMappings()
      ->willReturn([
        ['group' => 'role1'],
        ['group' => 'none'],
        ['group' => 'role2'],
      ]);
    $form_state = $this->prophesize(FormStateInterface::class);
    $form = $this->consumer->buildRowForm([], $form_state->reveal(), 0);

    $this->assertIsArray($form);
    $this->assertCount(1, $form);
    $this->assertArrayHasKey('group', $form);

  }

  /**
   * Tests the createConsumerTarget method.
   */
  public function testCreateConsumerTarget() {
    $res = $this->consumer->createConsumerTarget('test');
    $this->assertNull($res);
  }

  /**
   * Tests the getTokens method.
   */
  public function testgetTokens() {
    $res = $this->consumer->getTokens();

    $this->assertIsArray($res);
    $this->assertCount(4, $res);
    $this->assertArrayHasKey('@consumer_name', $res);
    $this->assertEquals('Group', $res['@consumer_name']);
    $this->assertArrayHasKey('@consumer_mappingDirections', $res);
    $this->assertEquals('', $res['@consumer_mappingDirections']);
    $this->assertArrayHasKey('@examples', $res);
    $this->assertEquals('', $res['@examples']);
    $this->assertArrayHasKey('@consumer_namePlural', $res);
    $this->assertEquals('Groups', (string) $res['@consumer_namePlural']);
  }

  /**
   * Tests the grantSingleAuthorization method.
   */
  public function testGrantSingleAuthorization() {
    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $group->label()
      ->willReturn('Group 1')
      ->shouldBeCalled($this->once());
    $this->groupStorage->load(1)
      ->willReturn($group->reveal())
      ->shouldBeCalled($this->once());

    $user = $this->prophesize('\Drupal\user\UserInterface');
    $user->id()
      ->willReturn(10)
      ->shouldBeCalled($this->once());

    $group_content = $this->prophesize('\Drupal\group\Entity\GroupContentInterface');

    $group->addMember($user->reveal())
      ->shouldBeCalled($this->once());
    $group->getContentByEntityId('group_membership', 10)
      ->willReturn([$group_content])
      ->shouldBeCalled($this->once());

    $group_role_1 = $this->prophesize('\Drupal\group\Entity\GroupRole');
    $this->groupRoleStorage->load('role1')
      ->willReturn($group_role_1->reveal())
      ->shouldBeCalled($this->once());

    $field_ref = $this->createMock('\Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem');
    $field_ref->method('__get')
      ->with('target_id')
      ->willReturn('role2');
    $role_list = [$field_ref];
    $group_content->get('group_roles')
      ->willReturn($role_list)
      ->shouldBeCalled($this->once());
    $group_content->set('group_roles', [['target_id' => 'role2'], ['target_id' => 'role1']])
      ->shouldBeCalled($this->once());
    $group_content->save()
      ->shouldBeCalled($this->once());

    $consumer_mapping = '1--role1';
    $profile_id = 'test';

    $this->consumer
      ->grantSingleAuthorization($user->reveal(), $consumer_mapping, $profile_id);
  }

  /**
   * Tests the grantSingleAuthorization method with invalid mapping.
   */
  public function testGrantSingleAuthorizationInvalidMapping() {
    $user = $this->prophesize('\Drupal\user\UserInterface');

    $consumer_mapping = '';
    $profile_id = 'test';

    $this->consumer
      ->grantSingleAuthorization($user->reveal(), $consumer_mapping, $profile_id);
  }

  /**
   * Tests the grantSingleAuthorization method with invalid role id.
   */
  public function testGrantSingleAuthorizationMissingRoleId() {

    $user = $this->prophesize('\Drupal\user\UserInterface');

    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $group->addMember($user->reveal())
      ->shouldBeCalled($this->once());

    $this->groupStorage->load(1)
      ->willReturn($group->reveal())
      ->shouldBeCalled($this->once());

    $consumer_mapping = '1--';
    $profile_id = 'test';

    $this->consumer
      ->grantSingleAuthorization($user->reveal(), $consumer_mapping, $profile_id);
  }

  /**
   * Tests the grantSingleAuthorization method with invalid group.
   */
  public function testGrantSingleAuthorizationMissingGroup() {

    $user = $this->prophesize('\Drupal\user\UserInterface');

    $this->groupStorage->load(1)
      ->willReturn(NULL)
      ->shouldBeCalled($this->once());
    $this->groupRoleStorage->load('role1')
      ->willReturn(NULL)
      ->shouldBeCalled($this->once());

    $consumer_mapping = '1--role1';
    $profile_id = 'test';

    $this->consumer
      ->grantSingleAuthorization($user->reveal(), $consumer_mapping, $profile_id);
  }

  /**
   * Tests the grantSingleAuthorization method with invalid role.
   */
  public function testGrantSingleAuthorizationMissingRole() {

    $user = $this->prophesize('\Drupal\user\UserInterface');

    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $group->addMember($user->reveal())
      ->shouldBeCalled($this->once());

    $this->groupStorage->load(1)
      ->willReturn($group->reveal())
      ->shouldBeCalled($this->once());
    $this->groupRoleStorage->load('role1')
      ->willReturn(NULL)
      ->shouldBeCalled($this->once());

    $consumer_mapping = '1--role1';
    $profile_id = 'test';

    $this->consumer
      ->grantSingleAuthorization($user->reveal(), $consumer_mapping, $profile_id);
  }

  /**
   * Tests the grantSingleAuthorization method with no memberships.
   */
  public function testGrantSingleAuthorizationNoMemberships() {

    $user = $this->prophesize('\Drupal\user\UserInterface');
    $user->id()
      ->willReturn(10)
      ->shouldBeCalled($this->once());

    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $group->addMember($user->reveal())
      ->shouldBeCalled($this->once());
    $group->getContentByEntityId('group_membership', 10)
      ->willReturn([])
      ->shouldBeCalled($this->once());

    $group_role_1 = $this->prophesize('\Drupal\group\Entity\GroupRole');
    $this->groupRoleStorage->load('role1')
      ->willReturn($group_role_1->reveal())
      ->shouldBeCalled($this->once());

    $this->groupStorage->load(1)
      ->willReturn($group->reveal())
      ->shouldBeCalled($this->once());

    $consumer_mapping = '1--role1';
    $profile_id = 'test';

    $this->consumer->grantSingleAuthorization($user->reveal(), $consumer_mapping, $profile_id);
  }

  /**
   * Tests the grantSingleAuthorization method with existing membership.
   */
  public function testGrantSingleAuthorizationExistingMembership() {
    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $this->groupStorage->load(1)
      ->willReturn($group->reveal())
      ->shouldBeCalled($this->once());

    $user = $this->prophesize('\Drupal\user\UserInterface');
    $user->id()
      ->willReturn(10)
      ->shouldBeCalled($this->once());

    $group_content = $this->prophesize('\Drupal\group\Entity\GroupContentInterface');

    $group->addMember($user->reveal())
      ->shouldBeCalled($this->once());
    $group->getContentByEntityId('group_membership', 10)
      ->willReturn([$group_content->reveal()])
      ->shouldBeCalled($this->once());

    $group_role_1 = $this->prophesize('\Drupal\group\Entity\GroupRole');
    $this->groupRoleStorage->load('role1')
      ->willReturn($group_role_1->reveal())
      ->shouldBeCalled($this->once());

    $field_ref = $this->createMock('\Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem');
    $field_ref->method('__get')
      ->with('target_id')
      ->willReturn('role1');
    $role_list = [$field_ref];
    $group_content->get('group_roles')
      ->willReturn($role_list)
      ->shouldBeCalled($this->once());

    $consumer_mapping = '1--role1';
    $profile_id = 'test';

    $this->consumer->grantSingleAuthorization($user->reveal(), $consumer_mapping, $profile_id);
  }

  /**
   * Tests the revokeGrants method.
   */
  public function testRevokeGrants() {
    $user = $this->prophesize('\Drupal\user\UserInterface');

    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $group->id()
      ->willReturn(1)
      ->shouldBeCalled($this->once());
    $group->label()
      ->willReturn('Group 1')
      ->shouldBeCalled($this->once());

    $group_role_1 = $this->prophesize('\Drupal\group\Entity\GroupRole');
    $group_role_1->id()
      ->willReturn('role1')
      ->shouldBeCalled($this->once());

    $group_role_2 = $this->prophesize('\Drupal\group\Entity\GroupRole');
    $group_role_2->id()
      ->willReturn('role2')
      ->shouldBeCalled($this->once());
    $group_role_2->label()
      ->willReturn('Role 2')
      ->shouldBeCalled($this->once());

    $group_content = $this->prophesize('\Drupal\group\Entity\GroupContentInterface');
    $group_content->set('group_roles', [['target_id' => 'role1']])
      ->shouldBeCalled($this->once());
    $group_content->save()
      ->shouldBeCalled($this->once());

    $group_membership = $this->prophesize('\Drupal\group\GroupMembership');
    $group_membership->getGroup()
      ->willReturn($group->reveal())
      ->shouldBeCalled($this->once());
    $group_membership->getGroupContent()
      ->willReturn($group_content->reveal())
      ->shouldBeCalled($this->once());
    $group_membership->getRoles()
      ->willReturn([$group_role_1->reveal(), $group_role_2->reveal()])
      ->shouldBeCalled($this->once());

    $this->membershipLoader->loadByUser($user->reveal(), NULL)
      ->willReturn([$group_membership->reveal()])
      ->shouldBeCalled($this->once());

    $context = [
      '1--role1',
    ];
    $profile_id = 'test';

    $this->consumer->revokeGrants($user->reveal(), $context, $profile_id);
  }

  /**
   * Tests the revokeGrants method with no group role.
   */
  public function testRevokeGrantsNoGroupRole() {
    $user = $this->prophesize('\Drupal\user\UserInterface');

    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $group->id()
      ->willReturn(2)
      ->shouldBeCalled($this->once());

    $group_content = $this->prophesize('\Drupal\group\Entity\GroupContentInterface');
    $group_content->set('group_roles', [])
      ->shouldBeCalled($this->once());
    $group_content->save()
      ->shouldBeCalled($this->once());

    $membership = $this->prophesize('\Drupal\group\GroupMembership');
    $membership->getGroup()
      ->willReturn($group->reveal())
      ->shouldBeCalled($this->once());
    $membership->getGroupContent()
      ->willReturn($group_content->reveal())
      ->shouldBeCalled($this->once());

    $this->membershipLoader->loadByUser($user->reveal(), NULL)
      ->willReturn([$membership->reveal()])
      ->shouldBeCalled($this->once());

    $context = [
      '1--role1',
    ];
    $profile_id = 'test';

    $this->consumer->revokeGrants($user->reveal(), $context, $profile_id);
  }

  /**
   * Tests the revokeGrants method with no group role.
   */
  public function testRevokeGrantsNoGroupRoleDelete() {
    $configuration = [
      'profile' => $this->profile->reveal(),
      'delete_membership' => TRUE,
    ];

    $this->consumer = GroupConsumer::create(
      \Drupal::getContainer(),
      $configuration,
      'authorization_group',
      [
        'label' => t('Groups'),
        'id' => 'group',
      ]);

    $user = $this->prophesize('\Drupal\user\UserInterface');

    $group = $this->prophesize('\Drupal\group\Entity\Group');
    $group->id()
      ->willReturn(2)
      ->shouldBeCalled($this->once());

    $group_content = $this->prophesize('\Drupal\group\Entity\GroupContentInterface');
    $group_content->delete()
      ->shouldBeCalled($this->once());

    $membership = $this->prophesize('\Drupal\group\GroupMembership');
    $membership->getGroup()
      ->willReturn($group->reveal())
      ->shouldBeCalled($this->once());
    $membership->getGroupContent()
      ->willReturn($group_content->reveal())
      ->shouldBeCalled($this->once());

    $this->membershipLoader->loadByUser($user->reveal(), NULL)
      ->willReturn([$membership->reveal()])
      ->shouldBeCalled($this->once());

    $context = [
      '1--role1',
    ];
    $profile_id = 'test';

    $this->consumer->revokeGrants($user->reveal(), $context, $profile_id);
  }

}
