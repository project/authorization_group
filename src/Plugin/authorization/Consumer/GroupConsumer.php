<?php

declare(strict_types=1);

namespace Drupal\authorization_group\Plugin\authorization\Consumer;

use Drupal\authorization\Consumer\ConsumerPluginBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Storage\GroupRoleStorageInterface;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Using Authorization to assign permissions to Groups.
 *
 * Groups are provided by the Group module.
 *
 * @AuthorizationConsumer(
 *   id = "authorization_group",
 *   label = @Translation("Groups")
 * )
 */
final class GroupConsumer extends ConsumerPluginBase {

  /**
   * Allow consumer target creation.
   *
   * @var bool
   */
  protected $allowConsumerTargetCreation = FALSE;

  /**
   * The membership loader service.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $membershipLoader;

  /**
   * The group storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $groupStorage;

  /**
   * The group role storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $groupRoleStorage;

  /**
   * Creates a group authorization consumer.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\group\GroupMembershipLoaderInterface $membership_loader
   *   The membership loader service.
   * @param \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $group_storage
   *   The group storage.
   * @param \Drupal\group\Entity\Storage\GroupRoleStorageInterface $group_role_storage
   *   The group role storage.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    GroupMembershipLoaderInterface $membership_loader,
    SqlEntityStorageInterface $group_storage,
    GroupRoleStorageInterface $group_role_storage) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->membershipLoader = $membership_loader;
    $this->groupStorage = $group_storage;
    $this->groupRoleStorage = $group_role_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('group.membership_loader'),
      $container->get('entity_type.manager')->getStorage('group'),
      $container->get('entity_type.manager')->getStorage('group_role')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['delete_membership'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove the user from the Group, if they no longer have any roles'),
      '#default_value' => $this->configuration['delete_membership'] ?? FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRowForm(array $form, FormStateInterface $form_state, $index): array {
    $row = [];
    $mappings = $this->configuration['profile']->getConsumerMappings();

    $group_options = ['none' => $this->t('- None -')];

    /** @var \Drupal\group\Entity\GroupInterface[] $groups */
    $groups = $this->groupStorage->loadMultiple();
    foreach ($groups as $group_id => $group) {
      $group_name = Html::escape($group->label());
      $group_type = $group->getGroupType();

      foreach ($group_type->getRoles() as $role) {
        $not_internal_or_anonymous = !$role->isInternal() || $role->isAnonymous();
        if ($not_internal_or_anonymous) {
          continue;
        }
        $group_options[$group_name][$group_id . '--' . $role->id()] = $role->label();
      }
    }

    $row['group'] = [
      '#type' => 'select',
      '#title' => $this->t('Group and Role'),
      '#options' => $group_options,
      '#default_value' => $mappings[$index]['group'] ?? 'none',
      '#description' => $this->t('Choose the Group to apply to the user.'),
    ];

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function createConsumerTarget(string $consumer): void {
    // @todo Implement createConsumerTarget() method.
    // We are not currently allowing auto-recreation of AD groups in Drupal.
    // No work to do.
  }

  /**
   * {@inheritdoc}
   */
  public function getTokens(): array {
    $tokens = parent::getTokens();

    // Reset the tokens for plurality.
    $tokens['@' . $this->getType() . '_namePlural'] = $this->label();
    $tokens['@' . $this->getType() . '_name'] = 'Group';
    return $tokens;
  }

  /**
   * {@inheritdoc}
   */
  public function grantSingleAuthorization(UserInterface $user, $consumer_mapping, string $profile_id): void {
    $invalid_mapping = empty($consumer_mapping) || $consumer_mapping === 'none';
    if ($invalid_mapping) {
      return;
    }

    [$group_id, $role_id] = explode('--', $consumer_mapping);
    /** @var \Drupal\group\Entity\GroupInterface $group */
    $group = $this->groupStorage->load($group_id);
    $role = $this->groupRoleStorage->load($role_id);

    if (!$group) {
      return;
    }

    $group->addMember($user);

    if (!$role) {
      return;
    }

    $assign_new_role = TRUE;

    $membership_roles = [];

    // Go through all the roles that the user currently have, within the
    // group.
    // If the user isn't assigned to the role yet, the assign it. Otherwise,
    // leave the roles as they are.
    $memberships = $group->getContentByEntityId('group_membership', $user->id());
    $invalid_memberships = empty($memberships) || !\is_array($memberships);
    if ($invalid_memberships) {
      return;
    }

    /** @var \Drupal\group\Entity\GroupContent $membership */
    foreach ($memberships as $membership) {
      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem[] $group_roles */
      $group_roles = $membership->get('group_roles');
      foreach ($group_roles as $group_role) {
        $membership_roles[] = ['target_id' => $group_role->target_id];
        if ($group_role->target_id == $role_id) {
          $assign_new_role = FALSE;
        }
      }

      if (!$assign_new_role) {
        continue;
      }
      $membership_roles[] = ['target_id' => $role_id];
      $this->messenger()->addStatus($this->t('You have been granted the role %role in the %group group', [
        '%role' => $role->label(),
        '%group' => $group->label(),
      ]));
      $membership->set('group_roles', $membership_roles);
      // @todo Handle potential exception here.
      $membership->save();

    }

  }

  /**
   * {@inheritdoc}
   */
  public function revokeGrants(UserInterface $user, array $context, string $profile_id): void {

    // Find the granted groups and roles from the applied grants (context).
    $granted_group_roles = [];
    foreach ($context as $mapping) {
      [$group_id, $role_id] = explode('--', $mapping);
      $granted_group_roles[$group_id][] = $role_id;
    }

    // Load all of the user's memberships.
    $memberships = $this->membershipLoader->loadByUser($user);

    foreach ($memberships as $membership) {
      $group = $membership->getGroup();

      // Wrapped membership content.
      $membership_content = $membership->getGroupContent();

      if (!isset($granted_group_roles[$group->id()])) {
        // Entire group was removed.
        // Either remove all roles or delete membership.
        $delete_membership = $this->configuration['delete_membership'] ?? FALSE;
        if ($delete_membership) {
          $membership_content->delete();
        }
        else {
          $membership_content->set('group_roles', []);
          $membership_content->save();
        }
      }
      else {
        // The group exists in the grants, so check all of the roles.
        $roles = $membership->getRoles();
        $roles_to_keep = [];

        foreach ($roles as $role) {
          if (\in_array($role->id(), $granted_group_roles[$group->id()])) {
            // Role exists in grants, so keep it.
            $roles_to_keep[] = ['target_id' => $role->id()];
          }
          else {
            $this->messenger()->addStatus($this->t('Your %role role, in the %group group, has been revoked', [
              '%role' => $role->label(),
              '%group' => $group->label(),
            ]));
          }
        }

        // Update the membership's roles.
        $membership_content->set('group_roles', $roles_to_keep);
        $membership_content->save();
      }
    }
  }

}
